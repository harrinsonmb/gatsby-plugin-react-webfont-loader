'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  inactive: 'inactive',
  active: 'active',
  loading: 'loading'
};