# Webfonts loader for Gatsby V2

## What?

A React wrapper for Typekit's [webfontloader](https://www.npmjs.com/package/webfontloader) NPM package.

## Why?

To load your webfonts more intelligently, avoid FOUT with them, and / or to ensure that they have REALLY loaded before doing something (use them in canvas etc). I hightly recommend you to reading the article about [critical FOFT](https://www.zachleat.com/web/comprehensive-webfonts/#critical-foft)

## How?

This is an example based in [layout.js](https://github.com/gatsbyjs/gatsby-starter-default/blob/v2/src/components/layout.js).

```javascript
import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

import Header from './header'
import './layout.css'

const Layout = ({ children, data }) => {
  const config = {
    google:{
      families: [
        'Philosopher:400,400i,700,700i',
        'Alegreya+Sans:400,400i,700,700i'
      ],
    }
  }
  
  return(
    <WebfontLoader config={config}>
      <StaticQuery
        query={graphql`
        query SiteTitleQuery {
          site {
            siteMetadata {
              title
            }
          }
        }
      `}
        render={data => (
          <>
            <Helmet
              title={data.site.siteMetadata.title}
              meta={[
                { name: 'description', content: 'Sample' },
                { name: 'keywords', content: 'sample, something' },
              ]}
            />
            <Header siteTitle={data.site.siteMetadata.title} />
            <div
              style={{
                margin: '0 auto',
                maxWidth: 960,
                padding: '0px 1.0875rem 1.45rem',
                paddingTop: 0,
              }}
            >
              {children}
            </div>
          </>
        )}
      />
    </WebfontLoader>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
```
