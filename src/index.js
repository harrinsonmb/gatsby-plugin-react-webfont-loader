import React from "react";
import PropTypes from "prop-types";
import statuses from "./statuses";

let WebFont = null

if(typeof window !== 'undefined'){
  WebFont = require('webfontloader');
}

const noop = () => {};

class WebfontLoader extends React.Component {
  state = {
    status: undefined
  };

  handleLoading = () => {
    this.setState({ status: statuses.loading });
  };

  handleActive = () => {
    this.setState({ status: statuses.active });
  };

  handleInactive = () => {
    this.setState({ status: statuses.inactive });
  };

  loadFonts = () => {
    if(WebFont !== null){
      WebFont.load({
        ...this.props.config,
        loading: this.handleLoading,
        active: this.handleActive,
        inactive: this.handleInactive
      });
    }else{
      console.info('Webfont is not being loaded, this is fine if you are executing from command line, for example, building a static site generator.');
    }
  }

  componentDidMount() {
    this.loadFonts();
  }

  componentDidUpdate(prevProps, prevState) {
    const { onStatus, config } = this.props;

    if (prevState.status !== this.state.status) {
      onStatus(this.state.status);
    }

    if (prevProps.config !== config) {
      this.loadFonts();
    }
  }

  render() {
    const { children } = this.props;
    return children || null;
  }
}

WebfontLoader.propTypes = {
  config: PropTypes.object.isRequired,
  children: PropTypes.element,
  onStatus: PropTypes.func.isRequired
};

WebfontLoader.defaultProps = {
  onStatus: noop
};

export default WebfontLoader;
